<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'anjanthebe' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7k3IRq&F5geQh<EL[i[qk9;y TOL?1;-W39|@PN,W[<.wR&&OJ$sy;r19zkQvhe~' );
define( 'SECURE_AUTH_KEY',  'Lhm-m?7iP#04@x$T>M>1)<@l$r:X:yDi4pGZSNNRxH3(m[ADT(SNKuEJSJ:Po2>X' );
define( 'LOGGED_IN_KEY',    '& #ih=n#T^_/iQcY:~=v=H1K$SnSX:+j+^p< ^+O8Js*Gae=->:wz;QZ-T2s5P?`' );
define( 'NONCE_KEY',        '$D>,wVZr}~-M2miewI |y}C]gWGI#Xv+htP:!qe[-wp)@s|*SY*(UoE:t1Ik|D,L' );
define( 'AUTH_SALT',        '.cv$U)y(/Qu&5) b7u7geOCVL9&z:WN3SV>OloVh^I&a9HUb*n@I^H|~l%[BhPt&' );
define( 'SECURE_AUTH_SALT', 'M*]yI|;N$}mL5V_>er[J#/H:+p(S4Rs=bi}Y0/fyk1FlbKNp_k-bXVS%wnd%rjls' );
define( 'LOGGED_IN_SALT',   '`c1FS1]uHEEY{:{AXPIf4yEF*%$DHlY@C7<~uhJ$PO^uvA<5;q[CepR,7s%6}PyH' );
define( 'NONCE_SALT',       '6@zaJ]J[&w+vImY7>)#UcurRWWhOk]{fm%S]L+9:1@G!J:f-[}][@7L_qpy&}^o#' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
